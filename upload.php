<?php

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('memory_limit', '-1');
set_time_limit(0);
if (empty($_POST['password']) || empty($_POST['login'])) {
    exit('не введены логи или пароль');
}
// Проверяем загружен ли файл
if (is_uploaded_file($_FILES["filename"]["tmp_name"])) {
    include __DIR__ . '/vendor/autoload.php';
    include __DIR__ . '/AntiCaptcha/Cuneiform.php';
    include __DIR__ . '/AntiCaptcha/Antigate.php';
    include __DIR__ . '/digiseller.php';
    $d = new digiseller();
    $d->login = $_POST['login'];
    $d->password = $_POST['password'];
    $start = (empty($_POST['start'])) ? 1 : (int) $_POST['start'];
    $d->auth();
    $d->groups();
    header('Content-Type: text/html; charset=utf-8');
    $result = '';
    foreach ($d->load($_FILES["filename"]["tmp_name"], $start) as $i => $value) {
        
        $d->my = $d->plati = null;
        list($plati, $my) = $d->create($value, $i);
        echo "позиция {$i}->my->";
        echo (filter_var($my,FILTER_VALIDATE_INT)) ? "успех <a href='https://my.digiseller.ru/inside/description.asp?id_d={$my}'>my</a>" : 'провал -> ' . $my . "\n";
        echo "<br>";
        echo "позиция {$i}->plati.ru->";
        echo (filter_var($plati,FILTER_VALIDATE_INT)) ? "успех <a href='https://my.digiseller.ru/inside/description.asp?id_d={$plati}'>my</a>"  : 'провал -> ' . $plati . "\n";
        echo "<br>";
    }
} else {
    echo("Ошибка загрузки файла");
}




//$d->create();
