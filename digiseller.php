<?php

use GuzzleHttp\Post\PostFile;

class digiseller {

    const
            url_auth = 'https://my.digiseller.ru/inside/ad.asp',
            url_auth_captcha = 'https://my.digiseller.ru/inside/gen_img.asp?nocash=',
            url_create_clear = 'https://my.digiseller.ru/inside/select_goods.asp?session=clear&rnd=',
            url_create_select_platform = 'https://my.digiseller.ru/inside/select_shop.asp',
            captcha_file = 'captcha.png';

    /**
     *
     * @var GuzzleHttp\Client 
     */
    public $client;

    /**
     *
     * @var array
     */
    public $cfg;

    /**
     *
     * @var phpQueryObject|QueryTemplatesSource|QueryTemplatesParse|QueryTemplatesSourceQuery
     */
    public $parser;

    /**
     *
     * @var  AntiCaptcha\Antigate
     */
    public $anticaptcha;

    /**
     *
     * @var PHPExcel
     */
    public $Excel;
    public $currency = [
        'WMZ' => 'USD',
        'WMR' => 'RUR',
        'WME' => 'EUR',
        'WMU' => 'UAH'
    ];
    public $dir;
    public $error;
    public $login;
    public $password;
    public $data;
    public $group;
    public $id;
    public $host;

    public function __construct() {
        $this->dir = __DIR__ . '/';
        $this->error = __DIR__ . '/errors/';
        $this->client = new GuzzleHttp\Client([
            'defaults' => [
                'cookies' => true,
                'debug' => false,
                'allow_redirects' => [
                    'max' => 10,
                    'strict' => false,
                    'referer' => true
                ],
                'timeout' => 5,
                'headers' => [
                    'User-Agent' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:30.0) Gecko/20100101 Firefox/30.0',
                    'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                    'Accept-Encoding' => 'gzip,deflate',
                    'Accept-Language' => 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
                ],
                'decode_content' => true,
                'verify' => false,
            ]
        ]);
        $this->anticaptcha = new AntiCaptcha\Antigate();
        $this->configure(include __DIR__ . '/config.php');
        $this->anticaptcha->min_len = 4;
        $this->anticaptcha->max_len = 8;
        $this->host = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']) . str_replace(__DIR__, '', $this->error) . '/';
    }

    public function configure(array $config) {
        foreach ($config as $name => $value) {
            switch ($name) {
                case 'dir':
                case 'error':
                    if (!is_dir($value)) {
                        mkdir($value);
                    }
                    $this->{$name} = $value;

                    break;
                case 'apikey':
                    $this->anticaptcha->apikey = $value;
                    break;
                case 'login':
                case 'password':
                    if (empty($value)) {
                        throw new Exception('не введен ' . $name);
                    }
                    $this->{$name} = $value;
                    break;
                default:
                    $this->{$name} = $value;
                    break;
            }
        }
    }

    public function is_auth($html) {
        $query = phpQuery::newDocument(mb_convert_encoding($html, "utf-8", "windows-1251"));
        return ($query->find('span.heading')->html() !== "Вход в личный кабинет");
    }

    public function auth() {
        $request = $this->client->get(self::url_auth);
        if ($this->is_auth($request->getBody())) {
            return;
        }
        $captcha_file = __DIR__ . '/' . self::captcha_file;
        $this->client->get(self::url_auth_captcha . rand(1e4, 99999), ['save_to' => $captcha_file]);
        while (true) {
            $result = $this->anticaptcha->exec($captcha_file);
            $this->client->get(self::url_auth_captcha . rand(1e4, 99999), ['save_to' => $captcha_file]);
            $request = $this->client->post(self::url_auth, [
                'query' => [
                    'owner' => '',
                    'returnpage' => '',
                    'redirecturl' => ''
                ],
                'body' => [
                    'login' => $this->login,
                    'password' => $this->password,
                    'turingnum' => $result,
                ]
            ]);
            if ($this->is_auth($request->getBody())) {
                return;
            }
        }
    }

    public function load($file, $start = 2, $end = null) {
        $list = [
            'name' => 'A',
            'description' => 'B',
            'info' => 'C',
            'cost' => 'D',
            'currency' => 'E',
            'commission' => 'F',
            'bonus' => 'G',
            'return' => 'H',
            'images' => [
                'I', 'J', 'K'
            ],
            'file' => 'L',
            'group_plati' => 'M',
            'group_my' => 'N'
        ];
        $result = [];
        $this->Excel = PHPExcel_IOFactory::load($file);
        $worksheet = $this->Excel->setActiveSheetIndex(0);
        $highestRow = ($end === null) ? $worksheet->getHighestRow() : $end;
        for ($row = $start; $row <= $highestRow; $row++) {
            foreach ($list as $name => $cellIndex) {
                if (is_string($cellIndex)) {
                    $cell = $worksheet->getCell($cellIndex . $row);
                    $value = trim($cell->getValue());

                    $result[$row][$name] = $value;
                } else {
                    foreach ($cellIndex as $i => $col) {
                        $cell = $worksheet->getCell($col . $row);
                        $result[$row][$name][$i] = trim($cell->getValue());
                    }
                }
            }
        }
        $this->Excel = null;
        return $result;
    }

    public $plati;
    public $my;

    /**
     *
     * @var array
     */
    public $groups_plati;

    /**
     *
     * @var array
     */
    public $groups_my;

    public function groups() {
        if ($this->groups_plati === null) {
            $request = $this->client->get(self::url_create_clear . rand(1e5, 1e6 - 1));
            $request = $this->client->post('https://my.digiseller.ru/inside/select_goods.asp?id_d=', [
                'body' => [
                    'CategoryAndForm' => 'digi_file',
                    'button' => mb_convert_encoding('продолжить', "utf-8", "windows-1251"),
                ],
//                'save_to'=>'groups.html'
            ]);
            $request = $this->client->get('https://my.digiseller.ru/inside/select_razdel.asp?owner=1&myshop=on&id_d=');
            $query = phpQuery::newDocument($request->getBody());
            $result = [];
            $query = phpQuery::newDocument(mb_convert_encoding($query->find('nobr')->html(), "utf-8", "windows-1251"));
            foreach ($query->find('input') as $i => $input) {
                $current = pq($input);
                $result[$current->val()] = trim($current->parent()->find('label')->text());
            }
            $this->groups_plati = $result;
        }
        if ($this->groups_my === null) {
            $request = $this->client->get(self::url_create_clear . rand(1e5, 1e6 - 1));
            $request = $this->client->post('https://my.digiseller.ru/inside/select_goods.asp?id_d=', [
                'body' => [
                    'CategoryAndForm' => 'digi_file',
                    'button' => mb_convert_encoding('продолжить', "utf-8", "windows-1251"),
                ],
//                'save_to'=>'groups.html'
            ]);
            $request = $this->client->get('https://my.digiseller.ru/inside/select_razdel.asp?owner=0&myshop=on&id_d=');
            $query = phpQuery::newDocument($request->getBody());
            $result = [];
            $query = phpQuery::newDocument(mb_convert_encoding($query->find('nobr')->html(), "utf-8", "windows-1251"));
            foreach ($query->find('input') as $i => $input) {
                $current = pq($input);
                $result[$current->val()] = trim($current->parent()->find('label')->text());
            }
            $this->groups_my = $result;
        }
        return $this;
    }

    /**
     * 
     * @param string $i
     * @return array
     */
    public function error($i) {
        $url = $this->host . $i . '.html';
        return [
            $this->error . $i . '.html',
            "<a href='$url'>$url</a>"
        ];
    }

    public $debug = false;

    public function create(array $data, $i = 0) {
        try {
            if (!empty($data['group_plati'])) {
                $plati = $this->create_plati($data, $i);
            }
        } catch (\Exception $e) {
            $plati = ($this->debug) ? $e->getMessage() . '<br>' . $e->getTraceAsString() : $e->getMessage();
        }
        try {
            if (!empty($data['group_my'])) {
                $my = $this->create_my($data, $i);
            }
        } catch (\Exception $e) {
            $my = ($this->debug) ? $e->getMessage() . '<br>' . $e->getTraceAsString() : $e->getMessage();
        }
        return [$plati, $my];
    }

    public function create_plati(array $data, $i = 0) {
        $group = array_search($data['group_plati'], $this->groups()->groups_plati);
        if ($group === false) {
            throw new Exception('не найдена или не введина такая группа:' . $data['group_plati']);
        }
        if (!($currency = array_search($data['currency'], $this->currency)) && !in_array($data['currency'], array_keys($this->currency))) {
            throw new Exception('не найдена или не введина такая денежная единица:' . $data['currency']);
        } else {
            $currency = ($currency) ? $currency : $data['currency'];
//            echo "\n\n\n$currency\n\n\n";
        }
        $file = $this->dir . $data['file'];
        if (!file_exists($file)) {
            throw new Exception('не найден или не указан файл:' . $file);
        }

        $button = mb_convert_encoding('продолжить', "utf-8", "windows-1251");

        $request = $this->client->get(self::url_create_clear . rand(1e5, 1e6 - 1));
        $request = $this->client->post('https://my.digiseller.ru/inside/select_goods.asp?id_d=', [
            'body' => [
                'CategoryAndForm' => 'digi_file',
                'button' => $button,
            ],
        ]);
        $request = $this->client->get('https://my.digiseller.ru/inside/select_razdel.asp?owner=1&myshop=on&id_d=');
        $request = $this->client->post('https://my.digiseller.ru/inside/select_razdel.asp?owner=1&myshop=on&id_d=', [
            'body' => [
                'Razdel' => $group,
                'button' => $button,
            ],
        ]);
        $request = $this->client->get('https://my.digiseller.ru/inside/new_description.asp', [
            'query' => [
                'id_r' => $group,
                'id_n' => ''
            ]
        ]);
        $body = [
            'TypeGood' => '2',
            'PropGood' => '1',
            'txt_NameGood' => iconv("utf-8", "windows-1251", $data['name']),
            'Price' => $data['cost'],
            'BaseCurrency' => $currency,
            'MaxPrice' => '0',
            'CreditPrice' => '0',
            'CreditPeriod' => '0',
            'CommissAgent' => '0',
            'GiftPercent' => '5',
            'txt_InfoGood' => iconv("utf-8", "windows-1251", $data['description']),
            'txt_AddInfo' => iconv("utf-8", "windows-1251", $data['info']),
            'button' => iconv("utf-8", "windows-1251", 'продолжить'),
            'Owner' => '1',
            'ID_R' => $group,
            'ID_N' => '',
        ];
        if (!empty($data['bonus'])) {
            if ((stripos('да', $data['bonus']) !== false)) {
                $body['GiftCommiss'] = 'ON';
                $body['GiftPercent'] = '0';
            } elseif (ctype_alnum($data['bonus'])) {
                $body['GiftCommiss'] = 'ON';
                $body['GiftPercent'] = $data['bonus'];
            }
        }
        if (!empty($data['return'])) {
            if ((stripos('да', $data['return']) !== false)) {
                $body['MoneyBack'] = 'ON';
                $body['MoneyBack_Days'] = '0';
            } elseif (ctype_alnum($data['return'])) {
                $body['MoneyBack'] = 'ON';
                $body['MoneyBack_Days'] = $data['return'];
            }
        }
        if (!empty($data['commission']) && ($data['commission'] > 0 && (stripos('нет', $data['commission']) === false))) {
            $body['CommissAgent'] = $data['commission'];
        }

        list($fileError, $link) = $this->error($i . '_plati');
        $request = $this->client->post('https://my.digiseller.ru/inside/new_description.asp', [
            'query' => [
                'id_r' => $group,
                'id_n' => ''
            ],
            'save_to' => $fileError,
            'body' => $body
        ]);
        $query = [];
        parse_str(parse_url($request->getEffectiveUrl(), PHP_URL_QUERY), $query);
        if (!isset($query['id_d']) || !ctype_digit($query['id_d'])) {
            throw new Exception('ошибка в обработке ' . $link);
        }
        $this->plati = $id = $query['id_d'];
        $request = $this->client->post('https://my.digiseller.ru/inside/import_gt2_preview.asp', [
            'query' => [
                'id_d' => $id,
                'rnd' => mt_rand(1e5, 1e6 - 1)
            ],
            'body' => [
                'UploadFile' => new PostFile('UploadFile', fopen($file, 'r'))
            ]
        ]);
        $request = $this->client->post('https://my.digiseller.ru/inside/import_gt2_preview.asp', [
            'query' => [
                'id_d' => $id,
                'action' => 'fillup'
            ]
        ]);
        $request = $this->client->get('https://my.digiseller.ru/inside/preview.asp', [
            'query' => [
                'id_d' => $id,
            ]
        ]);
        $imgArray = [];
        foreach ($data['images'] as $img) {
            if (!empty($img)) {
                $file = $this->dir . $img;
                if (file_exists($file)) {
                    $this->img($id, $file);
                } else {
                    $imgArray[] = $img;
                }
            }
        }
        return $id;
    }

    public function create_my(array $data, $i = 0) {
        $group = array_search($data['group_my'], $this->groups()->groups_my);
        if ($group === false) {
            throw new Exception('не найдена или не введина такая группа:' . $data['group_my']);
        }
        if (!($currency = array_search($data['currency'], $this->currency)) && !in_array($data['currency'], array_keys($this->currency))) {
            throw new Exception('не найдена или не введина такая денежная единица:' . $data['currency']);
        } else {
            $currency = ($currency) ? $currency : $data['currency'];
//            echo "\n\n\n$currency\n\n\n";
        }
        $file = $this->dir . $data['file'];
        if (!file_exists($file)) {
            throw new Exception('не найден или не указан файл:' . $file);
        }

        $button = mb_convert_encoding('продолжить', "utf-8", "windows-1251");

        $request = $this->client->get(self::url_create_clear . rand(1e5, 1e6 - 1));
        $request = $this->client->post('https://my.digiseller.ru/inside/select_goods.asp?id_d=', [
            'body' => [
                'CategoryAndForm' => 'digi_file',
                'button' => $button,
            ],
        ]);
        $request = $this->client->post('https://my.digiseller.ru/inside/select_razdel.asp?owner=1&myshop=on&id_d=', [
            'body' => [
                'List' => $group,
                'button' => '%EF%F0%EE%E4%EE%EB%E6%E8%F2%FC'
            ],
        ]);
        $body = [
            'TypeGood' => '2',
            'PropGood' => '1',
            'txt_NameGood' => iconv("utf-8", "windows-1251", $data['name']),
            'Price' => $data['cost'],
            'BaseCurrency' => $currency,
            'MaxPrice' => '0',
            'CreditPrice' => '0',
            'CreditPeriod' => '0',
            'CommissAgent' => '0',
            'GiftPercent' => '5',
            'txt_InfoGood' => iconv("utf-8", "windows-1251", $data['description']),
            'txt_AddInfo' => iconv("utf-8", "windows-1251", $data['info']),
            'button' => iconv("utf-8", "windows-1251", 'продолжить'),
            'ID_R' => '0',
            'ID_N' => $group,
        ];
        if (!empty($data['bonus'])) {
            if ((stripos('да', $data['bonus']) !== false)) {
                $body['GiftCommiss'] = 'ON';
                $body['GiftPercent'] = '0';
            } elseif (ctype_alnum($data['bonus'])) {
                $body['GiftCommiss'] = 'ON';
                $body['GiftPercent'] = $data['bonus'];
            }
        }
        if (!empty($data['return'])) {
            if ((stripos('да', $data['return']) !== false)) {
                $body['MoneyBack'] = 'ON';
                $body['MoneyBack_Days'] = '0';
            } elseif (ctype_alnum($data['return'])) {
                $body['MoneyBack'] = 'ON';
                $body['MoneyBack_Days'] = $data['return'];
            }
        }
        if (!empty($data['commission']) && ($data['commission'] > 0 && (stripos('нет', $data['commission']) === false))) {
            $body['CommissAgent'] = $data['commission'];
        }

        list($fileError, $link) = $this->error($i . '_my');
        $request = $this->client->post('https://my.digiseller.ru/inside/new_description.asp', [
            'query' => [
                'id_r' => '',
                'id_n' => $group
            ],
            'save_to' => $fileError,
            'body' => $body
        ]);
        $query = [];
        parse_str(parse_url($request->getEffectiveUrl(), PHP_URL_QUERY), $query);
        if (!isset($query['id_d']) || !ctype_digit($query['id_d'])) {
            throw new Exception('ошибка в обработке ' . $link);
        }
        $this->my = $id = $query['id_d'];
        $rnd = mt_rand(1e5, 1e6 - 1);
        $request = $this->client->post('https://my.digiseller.ru/inside/import_gt2_preview.asp', [
            'query' => [
                'id_d' => $id,
                'rnd' => $rnd
            ],
            'body' => [
                'UploadFile' => new PostFile('UploadFile', fopen($file, 'r')),
                'id_d' => $id,
                'rnd' => $rnd
            ],
        ]);
        $request = $this->client->post('https://my.digiseller.ru/inside/import_gt2_preview.asp', [
            'query' => [
                'id_d' => $id,
                'action' => 'fillup'
            ],
            'body' => [
                'id_d' => $id,
                'action' => 'fillup'
            ],
        ]);
        $request = $this->client->get('https://my.digiseller.ru/inside/preview.asp', [
            'query' => [
                'id_d' => $id,
            ],
        ]);
        $imgArray = [];
        foreach ($data['images'] as $img) {
            if (!empty($img)) {
                $file = $this->dir . $img;
                if (file_exists($file)) {
                    $this->img($id, $file);
                } else {
                    $imgArray[] = $img;
                }
            }
        }
        return $id;
    }

    public function img($id, $file) {
        $request = $this->client->post('http://my.digiseller.ru/inside/add_preview.asp', [
            'query' => [
                'id_d' => $id,
            ],
            'save_to' => 'test_0.html',
            'body' => [
                'UploadFile' => new PostFile('UploadFile', fopen($file, 'r')),
                'id_d' => $id
        ]]);
    }

    public function config_save(array $config) {
        file_put_contents(__DIR__ . '/config.php', "<?php return " . var_export($config, TRUE) . "?>");
    }

}
