<?php

namespace AntiCaptcha;

class Antigate  {

    private
            $resource;
    public
            $filename,
            $apikey,
            $is_verbose = true,
            $domain = "antigate.com",
            $rtimeout = 5,
            $mtimeout = 120,
            $is_phrase = 0,
            $is_regsense = 0,
            $is_numeric = 1,
            $min_len = 5,
            $max_len = 8,
            $is_russian = 0;
    protected
            $result,
            $error,
            $waittime,
            $url,
            $status,
            $code,
            $captcha_id,
            $success = false;

    const
            OK = 0,
            CAPCHA_NOT_READY = 1,
            ERROR_KEY_DOES_NOT_EXIST = 2,
            ERROR_WRONG_ID_FORMAT = 3,
            ERROR_CAPTCHA_UNSOLVABLE = 4,
            ERROR_NOT_FILE = 5,
            ERROR_CURL = 6,
            ERROR_LIMIT_TIME = 7,
            ERROR_WRONG_USER_KEY = 8,
            ERROR_ZERO_BALANCE = 9,
            ERROR_NO_SLOT_AVAILABLE = 10,
            ERROR_ZERO_CAPTCHA_FILESIZE = 11,
            ERROR_TOO_BIG_CAPTCHA_FILESIZE = 12,
            ERROR_WRONG_FILE_EXTENSION = 13,
            ERROR_IMAGE_TYPE_NOT_SUPPORTED = 14,
            ERROR_IP_NOT_ALLOWED = 15;

    static $codeResponse = array(
        'CAPCHA_NOT_READY',
        'ERROR_KEY_DOES_NOT_EXIST',
        'ERROR_WRONG_ID_FORMAT',
        'ERROR_CAPTCHA_UNSOLVABLE',
        'ERROR_WRONG_USER_KEY',
        'ERROR_ZERO_BALANCE',
        'ERROR_NO_SLOT_AVAILABLE',
        'ERROR_ZERO_CAPTCHA_FILESIZE',
        'ERROR_TOO_BIG_CAPTCHA_FILESIZE',
        'ERROR_WRONG_FILE_EXTENSION',
        'ERROR_IMAGE_TYPE_NOT_SUPPORTED',
        'ERROR_IP_NOT_ALLOWED'
    );

    public function __construct() {
        $this->open();
    }

    public function __destruct() {
        $this->close();
    }

    public function open() {
        $this->resource = curl_init();
    }

    public function close() {
        curl_close($this->resource);
    }

    public function options() {
        return array(
            CURLOPT_URL => 'http://' . $this->domain . '/in.php',
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array(
                'method' => 'post',
                'key' => $this->apikey,
                'file' => new \CurlFile($this->filename, 'image/'.pathinfo($this->filename, PATHINFO_EXTENSION), pathinfo($this->filename, PATHINFO_BASENAME)),
                'phrase' => $this->is_phrase,
                'regsense' => $this->is_regsense,
                'numeric' => $this->is_numeric,
                'min_len' => $this->min_len,
                'max_len' => $this->max_len,
            ),
        );
    }

    public function handler() {
        if (in_array($this->result, self::$codeResponse)) {
            $this->status = constant(__CLASS__ . '::' . $this->result);
            $this->handlerCode($this->status);
        } else {
            list($status, $this->code) = explode('|', $this->result);
            $this->status = constant(__CLASS__ . '::' . $status);
            $this->handlerCode($this->status);
        }
    }

    protected function request() {
        $this->success = false;
        $this->captcha_id = null;
        if (!file_exists($this->filename)) {
            $this->handlerCode(self::ERROR_NOT_FILE);
        }
        curl_setopt_array($this->resource, $this->options());
        $this->result = curl_exec($this->resource);
        if (curl_errno($this->resource)) {
            $this->handlerCode(self::ERROR_CURL);
        }
        $this->handler();
    }

    protected function response($cache = true) {
        if (!$cache) {
            $this->url = 'http://' . $this->domain . '/res.php?key=' . $this->apikey . '&action=get&id=' . $this->captcha_id;
        }

        $this->result = file_get_contents($this->url);
        $this->handler();
    }

    public function exec($file = null) {
        if ($file) {
            $this->filename = $file;
        }
        $this->waittime = 0;
        $this->request();
        $this->response(false);

        while (!$this->success && $this->waittime < $this->mtimeout) {
            $this->waittime += $this->rtimeout;
            $this->response();
        }
        if ($this->waittime > $this->mtimeout) {
            $this->handlerCode(self::ERROR_LIMIT_TIME);
        }
        return $this->code;
    }

    protected function handlerCode($code) {
        $previous = null;
        switch ($code) {
            case self::OK:
                if ($this->captcha_id) {
                    $this->success = true;
                } else {
                    $this->captcha_id = $this->code;
                }
                $error = false;
                break;
            case self::CAPCHA_NOT_READY;
                $msg = 'капча еще не распознана, повторите запрос через 1-5 секунд';
                $error = false;
                sleep($this->rtimeout);
                break;
            case self::ERROR_CAPTCHA_UNSOLVABLE;
                $error = true;
                $msg = 'капчу не смогли разгадать 5 разных работников';
                break;
            case self::ERROR_NOT_FILE:
                $error = true;
                $msg = 'не найден файл каптчи';
                break;
            case self::ERROR_KEY_DOES_NOT_EXIST:
                $error = true;
                $msg = 'вы использовали неверный captcha ключ в запросе';
                break;
            case self::ERROR_WRONG_ID_FORMAT:
                $error = true;
                $msg = 'некорректный идентификатор капчи, принимаются только цифры';
                break;
            case self::ERROR_CURL:
                $error = true;
                $msg = 'проблемы с соединением';
                $previous = new \Exception(curl_error($this->resource), curl_errno($this->resource));
                break;
            case self::ERROR_LIMIT_TIME:
                $error = true;
                $msg = 'вышел лимит времени';
                break;
            case self::ERROR_WRONG_USER_KEY:
                $error = true;
                $msg = 'неправильный формат ключа учетной записи (длина не равняется 32 байтам)';
                break;
            case self::ERROR_ZERO_BALANCE:
                $error = true;
                $msg = 'нулевой либо отрицательный баланс';
                break;
            case self::ERROR_NO_SLOT_AVAILABLE:
                $error = true;
                $msg = 'нет свободных работников в данный момент, попробуйте позже либо повысьте свою максимальную ставку';
                break;
            case self::ERROR_ZERO_CAPTCHA_FILESIZE:
                $error = true;
                $msg = 'размер капчи которую вы загружаете менее 100 байт';
                break;
            case self::ERROR_TOO_BIG_CAPTCHA_FILESIZE:
                $error = true;
                $msg = 'ваша капча имеет размер более 100 килобайт';
                break;
            case self::ERROR_WRONG_FILE_EXTENSION:
                $error = true;
                $msg = 'ваша капча имеет неверное расширение, допустимые расширения jpg,jpeg,gif,png';
                break;
            case self::ERROR_IMAGE_TYPE_NOT_SUPPORTED:
                $error = true;
                $msg = 'Невозможно определить тип файла капчи, принимаются только форматы JPG, GIF, PNG';
                break;
            case self::ERROR_IP_NOT_ALLOWED:
                $error = true;
                $msg = 'Запрос с этого IP адреса с текущим ключом отклонен. Пожалуйста смотрите раздел управления доступом по IP';
                break;
        }
        if ($error) {
            throw new \Exception($msg, $code, $previous);
        }
    }

}