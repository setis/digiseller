<?php

namespace AntiCaptcha;

use \Exception;

Class Cuneiform {

    public $file, $command, $out;
    public
            $cmd,
            $descriptorspec,
            $pipes,
            $cwd,
            $env,
            $other_options;

    const
            ERROR_NOT_FILE = 1,
            ERROR_NOT_PROC_OPEN = 2,
            ERROR_RUN_PROC = 3;

    protected $resource;
    public $error;
    public $autoDir = true;

    public function __construct() {
        $this->init();
    }

//    public function __destruct() {
//        $this->close();
//    }

    public function exec($file) {
        if (!is_file($file)) {
            throw new Exception($this->handlerCode(self::ERROR_NOT_FILE), self::ERROR_NOT_FILE);
        }
        $this->file = $file;
        if ($this->autoDir) {
            $this->cwd = dirname($this->file);
        }
        $this->out = $this->out();
        $this->cmd = $this->command();
        $this->open();
        if (!is_resource($this->resource)) {
            throw new Exception($this->handlerCode(self::ERROR_NOT_PROC_OPEN), self::ERROR_NOT_PROC_OPEN);
        }
        $this->error = stream_get_contents($this->pipes[2]);
        if ($this->error) {
            throw new Exception($this->handlerCode(self::ERROR_RUN_PROC).' '.$this->error, self::ERROR_RUN_PROC);
        }

//        print_r($this);
        $this->close();
        return $this->result();
    }

    public function init() {
        $this->descriptorspec = array(
            0 => array("pipe", "r"), // stdin - канал, из которого дочерний процесс будет читать
            1 => array("pipe", "w"), // stdout - канал, в который дочерний процесс будет записывать 
            2 => array("pipe", "w") // stderr - файл для записи
        );
    }

    public function open() {
        $this->resource = proc_open($this->cmd, $this->descriptorspec, $this->pipes, $this->cwd, $this->env, $this->other_options);
    }

    public function close() {
        proc_close($this->resource);
    }

    public function command() {
        return 'cuneiform -o ' . '\'' . $this->out . '\'' . ' --fax ' . '\'' . $this->file . '\'';
    }

    public function out() {
        return pathinfo($this->file, PATHINFO_FILENAME) . '.txt';
    }

    public function result() {
        return trim(file_get_contents($this->cwd . DIRECTORY_SEPARATOR . $this->out));
    }

    public function handlerCode($code) {
        switch ($code) {
            case self::ERROR_NOT_FILE:
                $error = true;
                $msg = 'нет файла';
                break;
            case self::ERROR_NOT_PROC_OPEN:
                $error = true;
                $msg = 'не открыт процесс';
                break;
            case self::ERROR_RUN_PROC:
                $error = true;
                $msg = 'не может выполнить - ' . $this->error;
                break;
        }
        return $msg;
    }

}
